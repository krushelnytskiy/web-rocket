'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.createTable(
            'users',
            {
                id: {
                    type: Sequelize.INTEGER,
                    primaryKey: true,
                    autoIncrement: true
                },
                email: Sequelize.STRING,
                name: Sequelize.STRING,
                password: Sequelize.STRING,
                active: {
                    type: Sequelize.BOOLEAN,
                    defaultValue: true
                },
                createdAt: {
                    type: Sequelize.DATE
                },
                updatedAt: {
                    type: Sequelize.DATE
                }
            }
        );
    },
    down: function (queryInterface, Sequelize) {
         return queryInterface.dropTable('users');
    }
};
