let fs        = require('fs');
let path      = require('path');
let Sequelize = require('sequelize');
let env       = process.env.NODE_ENV || 'development';
let config    = require(path.join(__dirname, '.', 'config', 'connection.json'))[env];
let sequelize = new Sequelize(config.database, config.username, config.password, config);
let db        = {
    sequelize: sequelize,
    Sequelize: Sequelize
};

fs
.readdirSync(path.join(__dirname, '..', 'models'))
.filter(function(file) {
    return (file.indexOf(".") !== 0);
})
.forEach(function(file) {
    let model = sequelize.import(path.join(__dirname, '..', 'models', file));
    db[model.name] = model;
});

Object.keys(db).forEach(function(modelName) {
    if ("associate" in db[modelName]) {
        db[modelName].associate(db);
    }
});

module.exports = db;