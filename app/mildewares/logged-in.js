module.exports = function (req, res, next) {
    if (undefined !== req.session.user) {
        next();
    } else {
        res.render('error/404', { url: req.url });
    }
};