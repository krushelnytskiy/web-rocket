let express = require('express'),
    router  = express.Router();

router.get('/login', require('../mildewares/not-logged-in'), function (req, res) {
    res.render('user/login');
});

router.get('/register', require('../mildewares/not-logged-in'), function (req, res, err) {
    res.render('user/register');
});

router.get('/account', require('../mildewares/logged-in'), function (req, res, err) {
    res.render('user/account', {login: 'asdfdsf'});
});

router.get('/logout', require('../mildewares/logged-in'), function (req, res) {
    req.session.destroy();
    res.redirect('/login');
});

module.exports = router;