let express = require('express'),
    router  = express.Router();

router.post('/register', function (req, res) {

    let db = req.app.appRequire('database/db');
    db.User.create(
        {
            name     : req.body.login,
            password : req.body.password,
            email    : req.body.email,
        }
    ).then(
        function () {
            res.json({
                message: 'success'
            });
        },
        function () {
            res.json(
                {
                    error: 'Could not save user'
                }
            );
        }
    )
});

router.post('/login', function (req, res) {

    let db = req.app.appRequire('database/db');
    db.User.findOne(
        {
            where: {
                password : req.body.password,
                email    : req.body.email,
            }
        }
    ).then(
        function (user) {
            if (null !== user) {
                req.session.user = user.id;
                req.session.save();
                res.json(req.session);
            } else {
                res.json({
                    success: false
                });
            }
        },
        function () {
            res.json(
                {
                    error: 'Could not find user'
                }
            );
        }
    )

});

module.exports = router;