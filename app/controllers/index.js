let express = require('express'),
    router  = express.Router();

router.use(function (req, res, next) {
    res.locals.session = req.session.user;
    next();
});

router.get('/', function (req, res) {
    res.render('index');
});

router.use(require('./user'));
router.use('/api', require('./api'));

module.exports = router;