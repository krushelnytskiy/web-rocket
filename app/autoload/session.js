let session = require('express-session');

module.exports = function (app) {
    app.use(session({
        secret: 'user_session',
        resave: false,
        saveUninitialized: true
    }));
};