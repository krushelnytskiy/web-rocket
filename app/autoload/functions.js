let path = require('path');

module.exports = function (app) {

    app.appRequire = function (file) {
        return require(path.join(__dirname, '..', file));
    };

};