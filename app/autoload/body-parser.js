module.exports = function (app) {
    let bodyParser = require('body-parser');
    app.use(bodyParser.json());
};