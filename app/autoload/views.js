module.exports = function (app) {
    let express = require('express');
    app.set('views', './app/views');
    app.set('view engine', 'jade');
    app.use(express.static('assets'));
};