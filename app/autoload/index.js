module.exports = [
    require('./session'),
    require('./views'),
    require('./404handler'),
    require('./body-parser'),
    require('./functions'),
];