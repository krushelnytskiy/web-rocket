module.exports = function(sequelize, Sequelize) {
    return sequelize.define("User",
        {
            email: Sequelize.STRING,
            name: Sequelize.STRING,
            password: Sequelize.STRING,
            active: Sequelize.BOOLEAN,
        },
        {
        classMethods: {
            tableName: 'users'
        }
    });
};