let app = angular.module('application', []);

app.controller('LoginController', ['$http', function ($http) {

    this.user = {
        email: '',
        password: '',
    };

    this.submitLogin = function () {

        $http({
            url: '/api/user/login',
            method: "POST",
            data: this.user
        })
        .then(
            function(response) {
                if (response.data.hasOwnProperty('errors')) {
                    this.errors = response.data['errors'];
                    return false;
                } else {
                    console.log(response.data);
                }
            },
            function(response) { // optional
                // failed
            }
        );
    };

}]);

app.controller('RegisterController', ['$http', function($http) {

    this.user = {
        login: '',
        email: '',
        password: ''
    };

    this.submitRegister = function () {
        $http({
            url: '/api/user/register',
            method: 'POST',
            data: this.user
        })
        .then(
            function (response) {
                if (response.data.hasOwnProperty('errors')) {
                    this.errors = response.data['errors'];
                    return false;
                } else {
                    console.log(response.data);
                }
            },
            function(response) { // optional
                // failed
            }
        );
    };

}]);