#!/usr/bin/env bash

#Install tools
function InstallTools () {
    sudo yum install -y tar bzip2 git zip unzip gcc gcc-c++
}

#Install nodeJS
function InstallNodeJS() {
    curl -sL https://rpm.nodesource.com/setup_7.x | sudo -E bash -
    sudo yum install -y nodejs npm
}

#Install MariaDB
function InstallMariaDB() {
    sudo yum -y install mariadb-server mariadb-client
    sudo systemctl enable mariadb
    sudo systemctl start mariadb
}

#Install project
function InstallProject() {
    cd /vagrant/web-rocket
    sudo chmod -R 777 modules
    mysql -u root -e "CREATE DATABASE IF NOT EXISTS `web-rocket`"
    echo "export PATH=$PATH:/vagrant/web-rocket/node_modules/.bin" >> ~/.bashrc
    . ~/.bashrc
    sequelize db:migrate \
        --config modules/models/config/connection.json \
        --options-path=modules/models/config/options.json \
        --env development
    sudo npm install
    sudo npm start
}

#Launch
InstallTools
InstallNodeJS
InstallMariaDB
InstallProject